package Vista;


import Componentes.Tabla;

import javax.swing.JToolBar;
import javax.swing.JButton;
import javax.swing.JFrame;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.BorderLayout;
import javax.swing.JScrollPane;

public class tablaLibro extends JFrame {
	private static final long serialVersionUID = 1L;
	public Tabla tablaL;
	
	public tablaLibro() {
		setLayout(new BorderLayout(0, 0));
		setTitle("Libros");
		
		JToolBar toolBar = new JToolBar();
		add(toolBar, BorderLayout.NORTH);
		
		JButton btnNewButton = new JButton("Buscar");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			
			}
		});
		toolBar.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Nuevo");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			}
		});
		toolBar.add(btnNewButton_1);
		
		JScrollPane scrollPane = new JScrollPane();
		
		
		this.add(scrollPane);
		
		Object[] cabecera= {"Id","Titulo","Autor","G�nero","Editorial"};
		Class<?>[] tipos= {Integer.class, String.class, String.class, String.class, String.class};
		Integer[] medidas= {50, 170, 180, 100, 120};
		
		tablaL = new Tabla(cabecera, tipos, medidas, null, null, null);
		tablaL.setResizableColumn(0, false);
		scrollPane.setViewportView(tablaL.tabla);
		
		
	}


}
