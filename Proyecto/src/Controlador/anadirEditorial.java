package Controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import Modelo.Editorial;

public class anadirEditorial {
	
	public Vista.anadirEditorial anadirEdit;
	public Modelo.Editorial editorial;
	
	
	public anadirEditorial(Editorial e) {
		anadirEdit = new Vista.anadirEditorial();
		this.editorial = e;
		inicializar();
		
		anadirEdit.btnAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (editorial == null) {
					
					editorial = Modelo.Editorial.create(anadirEdit.textNombre.getText());
					
				} else {
					editorial.nombre = anadirEdit.textNombre.getText();
					editorial.update();
				}
				Controlador.tablaEditoriales.cargarTabla();
				inicializar();
				
			}
		});
		
		anadirEdit.btnEliminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (editorial != null) {
					if (Varios.Datos.mConfirmacion("�Est�s seguro de que quieres borrar este editorial?") == JOptionPane.YES_OPTION) {
					editorial.delete();
					editorial = null;
					Controlador.tablaEditoriales.cargarTabla();
					inicializar();
					}
				}
			}
		});
		
		anadirEdit.btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				anadirEdit.dispose();
			}
		});
	}
	
	public void inicializar() {
		if (editorial != null) {
			anadirEdit.textId.setText(editorial.id+"");
			anadirEdit.textNombre.setText(editorial.nombre);
		} else {
			anadirEdit.textId.setText("");
			anadirEdit.textNombre.setText("");
		}	
	}
}
