package Controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Principal {
	
	public Vista.Principal prin;
	
	public Principal() {
		prin = new Vista.Principal();
	
		prin.menuAutores.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				 new Controlador.tablaAutores();
			}
		});
		
		prin.menuEditoriales.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				 new Controlador.tablaEditoriales();
			}
		});
		
	}
	
}
