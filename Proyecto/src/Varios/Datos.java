package Varios;

import javax.swing.JOptionPane;

public class Datos {
	public static boolean debug=false;
	
	public static void mError(String m) {
		JOptionPane.showMessageDialog(null, "Error: "+m, "Error", JOptionPane.ERROR_MESSAGE);
	}
	public static void mInfo(String m) {
		JOptionPane.showMessageDialog(null, "Información: "+m, "Información", JOptionPane.INFORMATION_MESSAGE);
	}
	public static int mConfirmacion(String m) {
		return JOptionPane.showConfirmDialog(null, m, "Confirmación", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
	}

}
