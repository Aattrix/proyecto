package Vista;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;

public class anadirAutor extends JFrame {
	public JTextField textNombre;
	public JTextField textApellido;
	public JTextField textId;
	public JTextField textFecha;
	public JButton btnAceptar;
	public JButton btnCancelar;
	public JButton btnEliminar;
	
	public anadirAutor() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setVisible(true);
		setTitle("Editar eutor");
		setSize(300,350);
		setResizable(false);
		getContentPane().setLayout(null);
		
		JLabel labelNombre = new JLabel("Nombre");
		labelNombre.setBounds(10, 29, 46, 14);
		getContentPane().add(labelNombre);
		
		textNombre = new JTextField();
		textNombre.setBounds(10, 54, 132, 20);
		getContentPane().add(textNombre);
		textNombre.setColumns(10);
		
		JLabel labelApellido = new JLabel("Apellidos");
		labelApellido.setBounds(10, 103, 46, 14);
		getContentPane().add(labelApellido);
		
		textApellido = new JTextField();
		textApellido.setBounds(10, 128, 132, 20);
		getContentPane().add(textApellido);
		textApellido.setColumns(10);
		
		textId = new JTextField();
		textId.setBounds(177, 54, 86, 20);
		getContentPane().add(textId);
		textId.setColumns(10);
		
		JLabel labelId = new JLabel("ID");
		labelId.setBounds(177, 29, 46, 14);
		getContentPane().add(labelId);
		
		JLabel labelFecha = new JLabel("Fecha de nacimiento");
		labelFecha.setBounds(10, 174, 107, 14);
		getContentPane().add(labelFecha);
		
		textFecha = new JTextField();
		textFecha.setBounds(10, 199, 132, 20);
		getContentPane().add(textFecha);
		textFecha.setColumns(10);
		
		btnAceptar = new JButton("Aceptar");
		btnAceptar.setBounds(10, 230, 89, 23);
		getContentPane().add(btnAceptar);
		
		btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(174, 261, 89, 23);
		getContentPane().add(btnCancelar);
		
		btnEliminar = new JButton("Eliminar");
		btnEliminar.setBounds(10, 261, 89, 23);
		getContentPane().add(btnEliminar);
	}
}
