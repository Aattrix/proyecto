package Vista;

import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionEvent;
import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;

import Componentes.Tabla;
import Vista.tablaLibro;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.Color;
import javax.swing.UIManager;
import javax.swing.JToolBar;


public class Principal extends JFrame {
	 
	public JMenu menu;
	public JMenuItem menuLibros;
	public JMenuItem menuClientes;
	public JMenuItem menuEditoriales;
	public JMenuItem menuAutores;

	public Principal() {
		getContentPane().setBackground(SystemColor.menu);
		setTitle("Libreria");
		setResizable(false);
		setSize(675,500);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		getContentPane().setLayout(null);

		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		menu = new JMenu("Opciones");
		menuBar.add(menu);
		
		menuLibros = new JMenuItem("Libros");
		menu.add(menuLibros);
		
		menuEditoriales = new JMenuItem("Editoriales");
		menu.add(menuEditoriales);
		
		menuAutores = new JMenuItem("Autores");
		menu.add(menuAutores);
		
		menuClientes = new JMenuItem("Clientes");
		menu.add(menuClientes);
		
		setVisible(true);
		repaint();
		revalidate();
		
		
	}
}
