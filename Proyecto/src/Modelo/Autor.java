package Modelo;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;

import Varios.Conexion;
import Varios.Datos;
import Varios.Fecha;


public class Autor {
	
	public Integer id;
	public String nombre;
	public String apellido;
	public Fecha fechaNac;
	
	public Autor(Integer id, String n, String a, Fecha f) {
		this.id = id;
		this.nombre = n;
		this.apellido = a;
		this.fechaNac = f;
	}
	
	public int delete() {
		return Autor.delete(this.id);
	}
	
	public static int delete(int i) {
		Conexion.open();
		PreparedStatement stm;
		try {
			stm=Conexion.con.prepareStatement("delete from autores where id=?");
			stm.setInt(1, i);
			stm.executeUpdate();
			stm.close();
			return 0;
			
		} catch (SQLException e) {
			if (Datos.debug) e.printStackTrace();
			Datos.mError(e.getErrorCode()+" - "+e.getLocalizedMessage());
			return e.getErrorCode();
		}
		
	}
	
	public static Autor create (String n, String a, Fecha f) {
		Conexion.open();
		PreparedStatement insercion;
		ResultSet respuesta;
		try {
			insercion=Conexion.con.prepareStatement("insert into autores (nombre, apellidos, fecha_nac) values (?,?,?)", Statement.RETURN_GENERATED_KEYS);
			insercion.setString(1, n);
			insercion.setString(2, a);
			insercion.setDate(3, f.getFechaMysql());
		
			insercion.executeUpdate();
			respuesta=insercion.getGeneratedKeys();
			
			if (respuesta.next()) return Autor.load(respuesta.getInt(1));
			
		} catch (SQLException ex) {
			if (Datos.debug) ex.printStackTrace();
			Datos.mError(ex.getErrorCode()+" - "+ex.getLocalizedMessage());
		}
		return null;
		
	}
	
	public static Autor load(Integer i) {
		Conexion.open();
		PreparedStatement stm;
		ResultSet respuesta;
		
		try {
			stm=Conexion.con.prepareStatement("Select * from autores where id=?");
			stm.setInt(1, i);
			respuesta=stm.executeQuery();
			if (respuesta.next()) {
				return new Autor (respuesta.getInt("id"), 
									respuesta.getString("nombre"), 
									respuesta.getString("apellidos"),
									Fecha.toFecha(respuesta.getDate("fecha_nac")));
			}
			stm.close();
			
		} catch (SQLException e) {
			if (Datos.debug) e.printStackTrace();
			Datos.mError(e.getErrorCode()+" - "+e.getLocalizedMessage());
		}
		return null;
		
	}
	
	public void update() {
		Conexion.open();
		PreparedStatement stm;
		
		try {
			stm=Conexion.con.prepareStatement("Update autores set nombre=?, apellidos=?, fecha_nac=? where id=?");
			stm.setString(1,this.nombre);
			stm.setString(2,this.apellido);
			stm.setDate(3,this.fechaNac.getFechaMysql());
			stm.setInt(4,this.id);
			stm.executeUpdate();
			
			stm.close();
			
			
		} catch (SQLException e) {
			if (Datos.debug) e.printStackTrace();
			Datos.mError(e.getErrorCode()+" - "+e.getLocalizedMessage());
		}
		
	}
	public static LinkedList<Autor> find (Integer i, String n, String a){
		Conexion.open();
		PreparedStatement stm;
		ResultSet respuesta;
		int x=1;
		LinkedList<Autor> lista=new LinkedList<Autor>();
		
		String consulta="Select * from autores where ";
		if (i!=null) consulta+=" id=? and ";
		if (n!=null) consulta+=" nombre like ? and ";
		if (a!=null) consulta+=" apellidos like ? and ";
		if (a!=null) consulta+=" fecha_nac like ? and ";
		consulta+=" 1=1 ";
		
		try {
			stm=Conexion.con.prepareStatement(consulta);
			if (i!=null) { stm.setInt(x,i); x++; }
			if (n!=null) { stm.setString(x, n+"%"); x++; }
			if (a!=null) { stm.setString(x, a+"%"); x++; }
			
			respuesta=stm.executeQuery();
			 while(respuesta.next()) {
				 lista.add(new Autor (respuesta.getInt("id"), 
							respuesta.getString("nombre"), 
							respuesta.getString("apellidos"),
							Fecha.toFecha(respuesta.getDate("fecha_nac"))));
			 }
			 stm.close();
			
			
		} catch (SQLException e) {
			if (Datos.debug) e.printStackTrace();
			Datos.mError(e.getErrorCode()+" - "+e.getLocalizedMessage());
		}
		return lista;
	}
	
}
