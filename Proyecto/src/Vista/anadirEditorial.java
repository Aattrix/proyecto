package Vista;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import Modelo.Editorial;

public class anadirEditorial extends JFrame {

	public JTextField textNombre;
	public JTextField textId;
	public JButton btnAceptar;
	public JButton btnCancelar;
	public JButton btnEliminar;
	
	
	public anadirEditorial() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setVisible(true);
		setTitle("Editar editorial");
		setSize(310,200);
		setResizable(false);
		getContentPane().setLayout(null);
		
		JLabel labelNombre = new JLabel("Nombre");
		labelNombre.setBounds(10, 11, 46, 14);
		getContentPane().add(labelNombre);
		
		textNombre = new JTextField();
		textNombre.setBounds(10, 36, 132, 20);
		getContentPane().add(textNombre);
		textNombre.setColumns(10);
		
		textId = new JTextField();
		textId.setBounds(198, 36, 86, 20);
		getContentPane().add(textId);
		textId.setColumns(10);
		
		JLabel labelId = new JLabel("ID");
		labelId.setBounds(198, 11, 46, 14);
		getContentPane().add(labelId);
		
		btnAceptar = new JButton("Aceptar");
		btnAceptar.setBounds(20, 90, 89, 23);
		getContentPane().add(btnAceptar);
		
		btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(174, 121, 89, 23);
		getContentPane().add(btnCancelar);
		
		btnEliminar = new JButton("Eliminar");
		btnEliminar.setBounds(20, 121, 89, 23);
		getContentPane().add(btnEliminar);
	}
	
	
}
