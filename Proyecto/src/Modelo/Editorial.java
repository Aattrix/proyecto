package Modelo;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;

import Varios.Conexion;
import Varios.Datos;

public class Editorial {

	public Integer id;
	public String nombre;
	
	
	public Editorial(Integer id, String n) {
		this.id = id;
		this.nombre = n;
	}
	
	public int delete() {
		return Editorial.delete(this.id);
	}
	
	public static int delete(int i) {
		Conexion.open();
		PreparedStatement stm;
		try {
			stm=Conexion.con.prepareStatement("delete from editorial where id=?");
			stm.setInt(1, i);
			stm.executeUpdate();
			stm.close();
			return 0;
			
		} catch (SQLException e) {
			if (Datos.debug) e.printStackTrace();
			Datos.mError(e.getErrorCode()+" - "+e.getLocalizedMessage());
			return e.getErrorCode();
		}
		
	}
	
	public static Editorial create (String n) {
		Conexion.open();
		PreparedStatement insercion;
		ResultSet respuesta;
		try {
			insercion=Conexion.con.prepareStatement("insert into editorial (nombre) values (?)", Statement.RETURN_GENERATED_KEYS);
			insercion.setString(1, n);

		
			insercion.executeUpdate();
			respuesta=insercion.getGeneratedKeys();
			
			if (respuesta.next()) return Editorial.load(respuesta.getInt(1));
			
		} catch (SQLException ex) {
			if (Datos.debug) ex.printStackTrace();
			Datos.mError(ex.getErrorCode()+" - "+ex.getLocalizedMessage());
		}
		return null;
		
	}
	
	public static Editorial load(Integer i) {
		Conexion.open();
		PreparedStatement stm;
		ResultSet respuesta;
		
		try {
			stm=Conexion.con.prepareStatement("Select * from editorial where id=?");
			stm.setInt(1, i);
			respuesta=stm.executeQuery();
			if (respuesta.next()) {
				return new Editorial (respuesta.getInt("id"), 
									respuesta.getString("nombre"));
			}
			stm.close();
			
		} catch (SQLException e) {
			if (Datos.debug) e.printStackTrace();
			Datos.mError(e.getErrorCode()+" - "+e.getLocalizedMessage());
		}
		return null;
		
	}
	
	public void update() {
		Conexion.open();
		PreparedStatement stm;
		
		try {
			stm=Conexion.con.prepareStatement("Update editorial set nombre=?, where id=?");
			stm.setString(1,this.nombre);
			stm.setInt(2,this.id);
			stm.executeUpdate();
			
			stm.close();
			
			
		} catch (SQLException e) {
			if (Datos.debug) e.printStackTrace();
			Datos.mError(e.getErrorCode()+" - "+e.getLocalizedMessage());
		}
		
	}
	public static LinkedList<Editorial> find (Integer i, String n){
		Conexion.open();
		PreparedStatement stm;
		ResultSet respuesta;
		int x=1;
		LinkedList<Editorial> lista=new LinkedList<Editorial>();
		
		String consulta="Select * from editorial where ";
		if (i!=null) consulta+=" id=? and ";
		if (n!=null) consulta+=" nombre like ? and ";
		consulta+=" 1=1 ";
		
		try {
			stm=Conexion.con.prepareStatement(consulta);
			if (i!=null) { stm.setInt(x,i); x++; }
			if (n!=null) { stm.setString(x, n+"%"); x++; }
			
			respuesta=stm.executeQuery();
			 while(respuesta.next()) {
				 lista.add(new Editorial (respuesta.getInt("id"), 
							respuesta.getString("nombre")));
			 }
			 stm.close();
			
			
		} catch (SQLException e) {
			if (Datos.debug) e.printStackTrace();
			Datos.mError(e.getErrorCode()+" - "+e.getLocalizedMessage());
		}
		return lista;
	}
	
}
