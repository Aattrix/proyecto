package Modelo;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;

import Varios.Conexion;
import Varios.Datos;

public class Libro {
	
	public Integer id;
	public String titulo;
	public Integer idAutor;
	public Integer idGenero;
	public Integer idlibros;
	public Double precio; 
	
	public Libro(Integer id, String t, Integer idA, Integer idG, Integer idE, Double p) {
		this.id = id;
		this.titulo = t;
		this.idAutor = idA;
		this.idGenero = idG;
		this.idlibros = idE;
		this.precio = p;
	}
	
	public int delete() {
		return Libro.delete(this.id);
	}
	
	public static int delete(int i) {
		Conexion.open();
		PreparedStatement stm;
		try {
			stm=Conexion.con.prepareStatement("delete from libros where id=?");
			stm.setInt(1, i);
			stm.executeUpdate();
			stm.close();
			return 0;
			
		} catch (SQLException e) {
			if (Datos.debug) e.printStackTrace();
			Datos.mError(e.getErrorCode()+" - "+e.getLocalizedMessage());
			return e.getErrorCode();
		}
		
	}
	
	public static Libro create (String t, Integer idA, Integer idG, Integer idE, Double p) {
		Conexion.open();
		PreparedStatement insercion;
		ResultSet respuesta;
		try {
			insercion=Conexion.con.prepareStatement("insert into libros (Titulo, idautor, idgenero, ideditorial, Precio) values (?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
			insercion.setString(1, t);
			insercion.setInt(2, idA);
			insercion.setInt(3, idG);
			insercion.setInt(4, idE);
			insercion.setDouble(5, p);

		
			insercion.executeUpdate();
			respuesta=insercion.getGeneratedKeys();
			
			if (respuesta.next()) return Libro.load(respuesta.getInt(1));
			
		} catch (SQLException ex) {
			if (Datos.debug) ex.printStackTrace();
			Datos.mError(ex.getErrorCode()+" - "+ex.getLocalizedMessage());
		}
		return null;
		
	}
	
	public static Libro load(Integer i) {
		Conexion.open();
		PreparedStatement stm;
		ResultSet respuesta;
		
		try {
			stm=Conexion.con.prepareStatement("Select * from libros where id=?");
			stm.setInt(1, i);
			respuesta=stm.executeQuery();
			if (respuesta.next()) {
				return new Libro (respuesta.getInt("id"), 
								  respuesta.getString("Titulo"),
								  respuesta.getInt("idautor"),
								  respuesta.getInt("idgenero"),
								  respuesta.getInt("ideditorial"),
								  respuesta.getDouble("Precio"));
			}
			stm.close();
			
		} catch (SQLException e) {
			if (Datos.debug) e.printStackTrace();
			Datos.mError(e.getErrorCode()+" - "+e.getLocalizedMessage());
		}
		return null;
		
	}
	
	public void update() {
		Conexion.open();
		PreparedStatement stm;
		
		try {
			stm=Conexion.con.prepareStatement("Update libros set Titulo=?, where id=?");
			stm.setString(1,this.titulo);
			stm.setInt(2,this.id);
			stm.executeUpdate();
			
			stm.close();
			
			
		} catch (SQLException e) {
			if (Datos.debug) e.printStackTrace();
			Datos.mError(e.getErrorCode()+" - "+e.getLocalizedMessage());
		}
		
	}
	public static LinkedList<Libro> find (Integer i, String t, Integer idA,Integer idG,Integer idE, Double p){
		Conexion.open();
		PreparedStatement stm;
		ResultSet respuesta;
		int x=1;
		LinkedList<Libro> lista=new LinkedList<Libro>();
		
		String consulta="Select * from libros where ";
		if (i!=null) consulta+=" id=? and ";
		if (t!=null) consulta+=" Titulo like ? and ";
		if (idA!=null) consulta+=" idautor like ? and ";
		if (idG!=null) consulta+=" idgenero like ? and ";
		if (idE!=null) consulta+=" ideditorial like ? and ";
		if (p!=null) consulta+=" Precio like ?";
		consulta+=" 1=1 ";
		
		try {
			stm=Conexion.con.prepareStatement(consulta);
			if (i!=null) { stm.setInt(x,i); x++; }
			if (t!=null) { stm.setString(x, t+"%"); x++; }
			
			respuesta=stm.executeQuery();
			 while(respuesta.next()) {
				 lista.add(new Libro (respuesta.getInt("id"), 
						  respuesta.getString("Titulo"),
						  respuesta.getInt("idautor"),
						  respuesta.getInt("idgenero"),
						  respuesta.getInt("ideditorial"),
						  respuesta.getDouble("Precio")));
			 }
			 stm.close();
			
			
		} catch (SQLException e) {
			if (Datos.debug) e.printStackTrace();
			Datos.mError(e.getErrorCode()+" - "+e.getLocalizedMessage());
		}
		return lista;
	}
	
	
	
	
}
