package Controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;

import javax.swing.JOptionPane;

public class tablaAutores {

	public static Vista.tablaAutores aut;
	
	public tablaAutores() {
		aut = new Vista.tablaAutores();
		cargarTabla();
		
		aut.btnAnadir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				 new anadirAutor(null);
			}
		});
		
		aut.btnSeleccionar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new anadirAutor(Modelo.Autor.load((Integer)aut.tablaA.getValueSelected(0)));
			}
		});
		
		aut.btnEliminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (Varios.Datos.mConfirmacion("�Est�s seguro de que quieres borrar este autor?") == JOptionPane.YES_OPTION) {
				Modelo.Autor.load((Integer)aut.tablaA.getValueSelected(0)).delete();
				cargarTabla();
				}
			}
		});
		
	}
	
	public static void cargarTabla() {
		
		LinkedList<Modelo.Autor> lista = Modelo.Autor.find(null, null, null);		
		aut.tablaA.vaciar();
		
		for (Modelo.Autor a : lista) {
			Object[] fila = {a.id, a.nombre, a.apellido, a.fechaNac.getFecha()};
			aut.tablaA.modelo.addRow(fila);
		}
		
	}
	
}
