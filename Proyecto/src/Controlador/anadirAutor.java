package Controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import Modelo.Autor;
import Varios.Fecha;

public class anadirAutor {

	public Vista.anadirAutor anadirAut;
	public Modelo.Autor autor;
	
	
	public anadirAutor(Autor a) {
		anadirAut = new Vista.anadirAutor();
		this.autor = a;
		inicializar();
		
		anadirAut.btnAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (autor == null) {
					
					autor = Modelo.Autor.create(anadirAut.textNombre.getText(), anadirAut.textApellido.getText(), new Fecha(anadirAut.textFecha.getText()));
					
				} else {
					autor.nombre = anadirAut.textNombre.getText();
					autor.apellido = anadirAut.textApellido.getText();
					autor.fechaNac = new Fecha (anadirAut.textFecha.getText());
					autor.update();
				}
				Controlador.tablaAutores.cargarTabla();
				inicializar();
				
			}
		});
		
		anadirAut.btnEliminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (autor != null) {
					if (Varios.Datos.mConfirmacion("�Est�s seguro de que quieres borrar este autor?") == JOptionPane.YES_OPTION) {
					autor.delete();
					autor = null;
					Controlador.tablaAutores.cargarTabla();
					inicializar();
					}
				}
			}
		});
		
		anadirAut.btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				anadirAut.dispose();
			}
		});
	}
	
	public void inicializar() {
		if (autor != null) {
			anadirAut.textId.setText(autor.id+"");
			anadirAut.textNombre.setText(autor.nombre);
			anadirAut.textApellido.setText(autor.apellido);
			anadirAut.textFecha.setText(autor.fechaNac.getFecha());
		} else {
			anadirAut.textId.setText("");
			anadirAut.textNombre.setText("");
			anadirAut.textApellido.setText("");
			anadirAut.textFecha.setText("");
		}
		
	}
	
}
