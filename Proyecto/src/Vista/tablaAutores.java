package Vista;

import javax.swing.JFrame;
import javax.swing.JScrollPane;

import java.awt.BorderLayout;
import javax.swing.JToolBar;

import Componentes.Tabla;
import Varios.Fecha;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class tablaAutores extends JFrame{
	
	public static Tabla tablaA;
	public JButton btnAnadir;
	public JButton btnSeleccionar;
	public JButton btnEliminar;
	
	
	public tablaAutores() {
		setResizable(false);
		setSize(675,450);
		setTitle("Autores");
		getContentPane().setLayout(null);
		setVisible(true);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 11, 624, 348);
		getContentPane().add(scrollPane);
		
		btnAnadir = new JButton("A\u00F1adir");
		btnAnadir.setBounds(10, 365, 89, 23);
		getContentPane().add(btnAnadir);
		
		btnSeleccionar = new JButton("Seleccionar");
		btnSeleccionar.setBounds(109, 365, 89, 23);
		getContentPane().add(btnSeleccionar);
		
		btnEliminar = new JButton("Eliminar");
		btnEliminar.setBounds(208, 365, 89, 23);
		getContentPane().add(btnEliminar);
		
		//Tabla de autores
		Object[] cabecera= {"Id","Nombre","Apellidos","Fecha_nac"};
		Class<?>[] tipos= {Integer.class, String.class, String.class, Fecha.class};
		Integer[] medidas= {50, 120, 160, 180};
		
		tablaA = new Tabla(cabecera, tipos, medidas, null, null, null);
		tablaA.setResizableColumn(0, false);
		scrollPane.setViewportView(tablaA.tabla);
		
	}
}
