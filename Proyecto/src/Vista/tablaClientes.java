package Vista;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;

import Componentes.Tabla;

import java.awt.BorderLayout;

import javax.swing.JButton;

public class tablaClientes extends JFrame{
	
	private static final long serialVersionUID = 1L;
	public Tabla tablaC;
	
	public tablaClientes() {
		setLayout(new BorderLayout(0, 0));
		setTitle("Clientes");
		
		JToolBar toolBar = new JToolBar();
		add(toolBar, BorderLayout.NORTH);
		
		//Bot�n buscar cliente
		JButton btnNewButton = new JButton("Buscar");
		toolBar.add(btnNewButton);
		
		//Bot�n a�adir cliente
		JButton btnNewButton_1 = new JButton("Nuevo");
		toolBar.add(btnNewButton_1);
	
		
		JScrollPane scrollPane = new JScrollPane();
		this.add(scrollPane);
		
		//Tabla de clientes
		Object[] cabecera= {"Id","Nombre","Apellidos","Telefono"};
		Class<?>[] tipos= {Integer.class, String.class, String.class, String.class};
		Integer[] medidas= {50, 120, 160, 180};
		
		tablaC = new Tabla(cabecera, tipos, medidas, null, null, null);
		tablaC.setResizableColumn(0, false);
		scrollPane.setViewportView(tablaC.tabla);
	
	
	}



}
