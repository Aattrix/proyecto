package Vista;

import javax.swing.JFrame;
import javax.swing.JScrollPane;

import java.awt.BorderLayout;
import javax.swing.JToolBar;

import Componentes.Tabla;
import Varios.Fecha;

import javax.swing.JButton;

public class tablaEditoriales extends JFrame{

	public static Tabla tablaE;
	public JButton btnAnadir;
	public JButton btnSeleccionar;
	public JButton btnEliminar;
	
	
	public tablaEditoriales() {
		setResizable(false);
		setSize(350,450);
		setTitle("Editoriales");
		getContentPane().setLayout(null);
		setVisible(true);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 11, 324, 348);
		getContentPane().add(scrollPane);
		
		btnAnadir = new JButton("A\u00F1adir");
		btnAnadir.setBounds(10, 365, 89, 23);
		getContentPane().add(btnAnadir);
		
		btnSeleccionar = new JButton("Seleccionar");
		btnSeleccionar.setBounds(109, 365, 89, 23);
		getContentPane().add(btnSeleccionar);
		
		btnEliminar = new JButton("Eliminar");
		btnEliminar.setBounds(208, 365, 89, 23);
		getContentPane().add(btnEliminar);
		
		//Tabla de editoriales
		Object[] cabecera= {"Id","Nombre"};
		Class<?>[] tipos= {Integer.class, String.class};
		Integer[] medidas= {50, 120};
		
		tablaE = new Tabla(cabecera, tipos, medidas, null, null, null);
		tablaE.setResizableColumn(0, false);
		scrollPane.setViewportView(tablaE.tabla);
		
	}
}
