package Controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;

import javax.swing.JOptionPane;

import Vista.anadirEditorial;


public class tablaEditoriales {

	public static Vista.tablaEditoriales edit;
	
	public tablaEditoriales() {
		edit = new Vista.tablaEditoriales();
		cargarTabla();
		
		edit.btnAnadir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				 new Controlador.anadirEditorial(null);
			}
		});
		
		edit.btnSeleccionar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new Controlador.anadirEditorial(Modelo.Editorial.load((Integer)edit.tablaE.getValueSelected(0)));
			}
		});
		
		edit.btnEliminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (Varios.Datos.mConfirmacion("�Est�s seguro de que quieres borrar este autor?") == JOptionPane.YES_OPTION) {
				Modelo.Editorial.load((Integer)edit.tablaE.getValueSelected(0)).delete();
				cargarTabla();
				}
			}
		});
		
	}
	
	public static void cargarTabla() {
		
		LinkedList<Modelo.Editorial> lista = Modelo.Editorial.find(null, null);		
		edit.tablaE.vaciar();
		
		for (Modelo.Editorial a : lista) {
			Object[] fila = {a.id, a.nombre};
			edit.tablaE.modelo.addRow(fila);
		}
		
	}
	
}
